
#-------------------------[[ General game parameters ]]-------------------------
#-------------------------------------------------------------------------------

#the number of bots the game instantiates

export var NumBots   = 3

#this is the maximum number of search cycles allocated to *all* current path
# planning searches per update
export var MaxSearchCyclesPerUpdateStep = 1000

#the name of the default map
export var StartMap : String = "res://maps/Raven_DM1.map"

#cell space partitioning defaults
export var NumCellsX = 10
export var NumCellsY = 10

#how long the graves remain on screen
export var GraveLifetime = 5


#-------------------------[[ bot parameters ]]----------------------------------
#-------------------------------------------------------------------------------

export var Bot_MaxHealth : float = 100
export var Bot_MaxSpeed : float  = 1
export var Bot_Mass : float      = 1
export var Bot_MaxForce : float  = 1.0
export var Bot_MaxHeadTurnRate : float = 0.2
export var Bot_Scale : float      = 0.8

# special movement speeds (unused)
export var Bot_MaxSwimmingSpeed : float = Bot_MaxSpeed * 0.2
export var Bot_MaxCrawlingSpeed : float = Bot_MaxSpeed * 0.6

# the number of times a second a bot 'thinks' about weapon selection
export var Bot_WeaponSelectionFrequency = 2

# the number of times a second a bot 'thinks' about changing strategy
export var Bot_GoalAppraisalUpdateFreq = 4

# the number of times a second a bot updates its target info
export var Bot_TargetingUpdateFreq = 2

# the number of times a second the triggers are updated
export var Bot_TriggerUpdateFreq = 8

# the number of times a second a bot updates its vision
export var Bot_VisionUpdateFreq = 4

# note that a frequency of -1 will disable the feature and a frequency of zero
# will ensure the feature is updated every bot update


# the bot's field of view (in degrees)
export var Bot_FOV = 180

# the bot's reaction time (in seconds)
export var Bot_ReactionTime = 0.2

# how long (in seconds) the bot will keep pointing its weapon at its target
# after the target goes out of view
export var Bot_AimPersistance = 1

# how accurate the bots are at aiming. 0 is very accurate, (the value represents
# the max deviation in range (in radians))
export var Bot_AimAccuracy = 0.0

# how long a flash is displayed when the bot is hit
export var HitFlashTime = 0.2

# how long (in seconds) a bot's sensory memory persists
export var Bot_MemorySpan = 5

# goal tweakers
export var Bot_HealthGoalTweaker     = 1.0
export var Bot_ShotgunGoalTweaker    = 1.0
export var Bot_RailgunGoalTweaker    = 1.0
export var Bot_RocketLauncherTweaker = 1.0
export var Bot_AggroGoalTweaker      = 1.0


#-------------------------[[ steering parameters ]]-----------------------------
#-------------------------------------------------------------------------------

# use these values to tweak the amount that each steering force
# contributes to the total steering force
export var SeparationWeight       =   10.0;
export var WallAvoidanceWeight     =  10.0;
export var WanderWeight            =  1.0;
export var SeekWeight              =  0.5;
export var ArriveWeight            =  1.0;

# how close a neighbour must be before an agent considers it
# to be within its neighborhood (for separation)
export var ViewDistance            =  15.0;

# max feeler length
export var WallDetectionFeelerLength : float = 25.0 * Bot_Scale;

# used in path following. Determines how close a bot must be to a waypoint
# before it seeks the next waypoint
export var WaypointSeekDist   = 5;

#-------------------------[[ giver-trigger parameters ]]-----------------------------
#-------------------------------------------------------------------------------

# how close a bot must be to a giver-trigger for it to affect it
export var DefaultGiverTriggerRange = 10

# how many seconds before a giver-trigger reactivates itself
export var Health_RespawnDelay  = 10
export var Weapon_RespawnDelay  = 15



#-------------------------[[ weapon parameters ]]-------------------------------
#-------------------------------------------------------------------------------

export var Blaster_FiringFreq       = 3
export var Blaster_MaxSpeed		 = 5
export var Blaster_DefaultRounds    = 0 # not used, a blaster always has ammo
export var Blaster_MaxRoundsCarried = 0 # as above
export var Blaster_IdealRange		   = 50
export var Blaster_SoundRange	     = 100

export var Bolt_MaxSpeed : float    = 5
export var Bolt_Mass : float        = 1
export var Bolt_MaxForce : float    = 100.0
export var Bolt_Scale : float       = Bot_Scale
export var Bolt_Damage : float      = 1



export var RocketLauncher_FiringFreq       = 1.5
export var RocketLauncher_DefaultRounds      = 15
export var RocketLauncher_MaxRoundsCarried = 50
export var RocketLauncher_IdealRange  = 150
export var RocketLauncher_SoundRange  = 400

export var Rocket_BlastRadius = 20
export var Rocket_MaxSpeed    = 3
export var Rocket_Mass        = 1
export var Rocket_MaxForce    = 10.0
export var Rocket_Scale : float      = Bot_Scale
export var Rocket_Damage      = 10
export var Rocket_ExplosionDecayRate = 2.0   # how fast the explosion occurs (in secs)


export var RailGun_FiringFreq       = 1
export var RailGun_DefaultRounds      = 15
export var RailGun_MaxRoundsCarried = 50
export var RailGun_IdealRange  = 200
export var RailGun_SoundRange  = 400

export var Slug_MaxSpeed    = 5000
export var Slug_Mass        = 0.1
export var Slug_MaxForce    = 10000.0
export var Slug_Scale : float       = Bot_Scale
export var Slug_Persistance = 0.2
export var Slug_Damage      = 10



export var ShotGun_FiringFreq       = 1
export var ShotGun_DefaultRounds      = 15
export var ShotGun_MaxRoundsCarried = 50
export var ShotGun_NumBallsInShell  = 10
export var ShotGun_Spread           = 0.05
export var ShotGun_IdealRange  = 100
export var ShotGun_SoundRange  = 400

export var Pellet_MaxSpeed    = 5000
export var Pellet_Mass        = 0.1
export var Pellet_MaxForce    = 1000.0
export var Pellet_Scale : float      = Bot_Scale
export var Pellet_Persistance = 0.1
export var Pellet_Damage      = 1


  