#ifndef SCRIPTOR_H
#define SCRIPTOR_H
//-----------------------------------------------------------------------------
//
//  Name:   Scriptor.h
//
//  Author: Mykola Shatokhin
//
//  Desc:   class encapsulating the basic functionality necessary to read a
//          GDScript config resource
//-----------------------------------------------------------------------------
#ifdef CONNECT_DEFERRED
#undef CONNECT_DEFERRED
#endif

#include <core/Godot.hpp>
#include <gen/Node.hpp>
#include <gen/Resource.hpp>
#include <gen/ResourceLoader.hpp>

class Scriptor
{
private:

  godot::Ref<godot::Resource> resource;

public:

  Scriptor(){}

  ~Scriptor(){}

  void RunScriptFile(char* ScriptName)
  {
     godot::ResourceLoader* reLo = godot::ResourceLoader::get_singleton();
     resource = reLo->load(ScriptName);
  }

  int GetInt(char* VariableName)
  {
    return resource->get(VariableName);
  }
    
  double GetFloat(char* VariableName)
  {
    return resource->get(VariableName);
  }

  double GetDouble(char* VariableName)
  {
    return resource->get(VariableName);
  }

  std::string GetString(char* VariableName)
  {
    godot::String str = resource->get(VariableName);
    return std::string(str.utf8().get_data());
  }

  bool GetBool(char* VariableName)
  {
    return resource->get(VariableName);
  }
};

#endif

 
  

