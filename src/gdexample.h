#ifndef GDEXAMPLE_H
#define GDEXAMPLE_H

#include <Godot.hpp>
#include <Control.hpp>

#include "Raven_Game.h"

namespace godot {

class GDExample : public Control {
    GODOT_CLASS(GDExample, Control)

private:
    float time_passed;

    Raven_Game* g_pRaven;

public:
    static void _register_methods();

    GDExample();
    ~GDExample();

    void _init(); // our initializer called by Godot

    void _process(float delta);
    void _draw();
;};

}

#endif
