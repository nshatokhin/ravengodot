#include "gdexample.h"

using namespace godot;

void GDExample::_register_methods() {
    register_method("_process", &GDExample::_process);
    register_method("_draw", &GDExample::_draw);
}

GDExample::GDExample() {
    g_pRaven = new Raven_Game(this);
}

GDExample::~GDExample() {
    delete g_pRaven;
}

void GDExample::_init() {
    // initialize any variables here
    time_passed = 0.0;
}

void GDExample::_process(float delta) {
    g_pRaven->Update();

    update();
}

void GDExample::_draw() {
    g_pRaven->Render();
}
