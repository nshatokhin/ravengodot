#include "Goal_MoveToItem.h"

#include "..\Raven_Bot.h"
#include "../navigation/Raven_PathPlanner.h"
#include "Messaging/Telegram.h"
#include "../Raven_Messages.h"

#include "Goal_SeekToPosition.h"
#include "Goal_FollowPath.h"

void Goal_MoveToItem::Activate()
{
  m_iStatus = active;
  
  //make sure the subgoal list is clear.
  RemoveAllSubgoals();

  //request a path to the item
  if (m_pOwner->GetPathPlanner()->RequestPathToItem(m_iItemType))
  {
    //the bot may have to wait a few update cycles before a path is calculated
    //so for appearances sake it just moves forward a little
    AddSubgoal(new Goal_SeekToPosition(m_pOwner, m_pOwner->Pos() + m_pOwner->Facing()*20));
  }
}

//---------------------------- HandleMessage ----------------------------------
//-----------------------------------------------------------------------------
bool Goal_MoveToItem::HandleMessage(const Telegram& msg)
{
  //first, pass the message down the goal hierarchy
  bool bHandled = ForwardMessageToFrontMostSubgoal(msg);

  //if the msg was not handled, test to see if this goal can handle it
  if (bHandled == false)
  {
    switch(msg.Msg)
    {
    case Msg_PathReady:

      //clear any existing goals
      RemoveAllSubgoals();

      AddSubgoal(new Goal_FollowPath(m_pOwner,
                                     m_pOwner->GetPathPlanner()->GetPath()));

      return true; //msg handled


    case Msg_NoPathAvailable:

      m_iStatus = failed;

      return true; //msg handled

    default: return false;
    }
  }

  //handled by subgoals
  return true;
}