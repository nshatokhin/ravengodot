#ifndef GOAL_MOVE_TO_ITEM_H
#define GOAL_MOVE_TO_ITEM_H
#pragma warning (disable:4786)

#include "Goals/Goal_Composite.h"
#include "2D/Vector2D.h"
#include "../Raven_Bot.h"
#include "Raven_Goal_Types.h"

class Raven_Bot;


class Goal_MoveToItem : public Goal_Composite<Raven_Bot>
{
private:

  int  m_iItemType;

public:

  Goal_MoveToItem(Raven_Bot* pBot, int type) : Goal_Composite<Raven_Bot>(pBot, goal_move_to_item),
                                               m_iItemType(type)
  {}

  void Activate();

  int Process(){}

  void Terminate(){}

  //this goal is able to accept messages
  bool HandleMessage(const Telegram& msg);
};



#endif
